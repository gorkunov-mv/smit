require('dotenv').config();

const locals = require('./data/storage');
const env = process.env.NODE_ENV;
const root = process.env.SRC || 'frontend';
const dest = process.env.DEST || 'dist';
const build = process.env.BUILD || 'build';
const isProduction = env === 'production';

module.exports = {
  env,
  isProduction,
  locals,
  path: {
    root,
    dest,
    build
  }
};
