/**
 * modal
 */

import Component from '../../js/lib/component';
import Page from '../page/page';
import $ from 'jquery';
import VanillaModal from 'vanilla-modal';

const classList = {
  el: '.js-modal',
  inner: '.js-modal-inner',
  content: '.js-modal-content',
  template: '.js-modal-template',
  default: '.js-modal-default'
};

class Modal extends Component {
  init() {
    this.modal = new VanillaModal({
      modal: classList.template,
      modalInner: classList.inner,
      modalContent: classList.content,
      loadClass: 'modal-init',
      onBeforeOpen: () => {
        Page.bodyOverflowEnable();
      },
      onClose: () => {
        Page.bodyOverflowDisable();
        Page.blurFocus();
      },
      onOpen: () => {
        Page.setFocus(this.$template);
      }
    });
  }
  events() {
    this.$template.children().on('transitionend', (e) => false);
    this.$document.on('click.modal', '[data-modal-close]', () => this.close());
    this.$document.on('click.modal', '[data-modal]', (e) => this.handler(e));
    this.$document.on('click.modal', '[data-modal-ajax]', (e) => this.handler(e, true));
  }
  handler(e, ajax) {
    e.preventDefault();

    const $target = $(e.currentTarget);

    if (ajax) {
      this.load({ $target, url: $target.data('modal-ajax') }).then(() => {
        this.open();
      });
    } else {
      this.open(`#modal-${$target.data('modal')}`);
    }
  }
  load({ $target = null, url, data = {}, container = 'default' }) {
    const timeout = setTimeout(() => {
      if ($target) {
        $target.addClass('is-loading');
      }
    }, 800);

    return $.ajax(url, { data })
      .then(({ html }) => {
        this[`$${container}`].html(html);
      })
      .fail((err) => {
        console.error(err);
      })
      .always(() => {
        clearTimeout(timeout);
        if ($target) {
          $target.removeClass('is-loading');
        }
      });
  }
  open(id = '#modal-default') {
    this.modal.open(id);
  }
  close() {
    this.modal.close();
  }
}

export default Component.mount(Modal, {
  name: 'Modal',
  classList,
  singleton: true,
  state: {
    init: false
  }
});
