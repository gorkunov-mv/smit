/**
 * slider
 */

import Component from "../../js/lib/component";
import Swiper from "swiper";

const classList = {
  el: ".js-slider",
  container: ".js-slider-container",
  list: ".js-slider-list",
  item: ".js-slider-item",
  prev: ".js-slider-prev",
  next: ".js-slider-next",
  counter: ".js-slider-counter",
};

export default class Slider extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: 1,
        loop: false,
        speed: 800,
        autoplay: true,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        fadeEffect: {
          crossFade: true,
        },
        effect: "fade",
        navigation: {
          prevEl: this.$prev.get(0),
          nextEl: this.$next.get(0),
        },
        on: {
          init: () => {
            this.$el.addClass("is-inited");
          },
        },
      },
      this.$el.data("options")
    );

    this.swiper = new Swiper(this.$container.get(0), this.options);
  }
  events() {
    this.swiper.on("slideChange", () => {
      const index = this.swiper.activeIndex;
      const slidesAmount = this.swiper.slides.length;

      this.updateCounter(index + 1, slidesAmount);
    });
  }
  updateCounter(current, slidesAmount) {
    this.$counter.text(`${current} / ${slidesAmount}`);
  }
}

Component.mount(Slider, {
  name: "Slider",
  state: {},
  classList,
});
