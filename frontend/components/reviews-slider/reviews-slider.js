/**
 * reviews-slider
 */

import Component from "../../js/lib/component";
import Swiper from "swiper";

const classList = {
  el: ".js-reviews-slider",
  container: ".js-reviews-slider-container",
  list: ".js-reviews-slider-list",
  item: ".js-reviews-slider-item",
  prev: ".js-reviews-slider-prev",
  next: ".js-reviews-slider-next",
  counter: ".js-reviews-slider-counter",
};

export default class ReviewsSlider extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        speed: 400,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        navigation: {
          prevEl: this.$prev.get(0),
          nextEl: this.$next.get(0),
        },
        on: {
          init: () => {
            this.$el.addClass("is-inited");
          },
        },
      },
      this.$el.data("options")
    );

    this.swiper = new Swiper(this.$container.get(0), this.options);
  }
  events() {
    this.swiper.on("slideChange", () => {
      this.changeHandler();
    });
  }
  changeHandler() {
    this.updateCounter(this.swiper.realIndex + 1, this.$item.length);
  }
  updateCounter(current, slidesAmount) {
    this.$counter.text(`${current} / ${slidesAmount}`);
  }
}

Component.mount(ReviewsSlider, {
  name: "ReviewsSlider",
  state: {},
  classList,
});
