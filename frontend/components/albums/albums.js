/**
 * Albums
 */

import $ from 'jquery';
import Component from '../../js/lib/component';

const mockHTML =
  '<li class="albums__item col-4"><article class="album"><figure class="album__figure"><img class="album__image" src="/img/post-4.jpg" alt="Вот и подошла к концу выставка «Метал-Экспо 2019»"></figure><div class="album__body"><header class="album__header"><h3 class="album__title"><a class="album__link" href="/single.html" title="Вот и подошла к концу выставка «Метал-Экспо 2019»">Вот и подошла к концу выставка «Метал-Экспо 2019»</a></h3></header><div class="album__meta"><time class="album__date">01.11.2019</time></div></div></article></li><li class="albums__item col-4"><article class="album"><figure class="album__figure"><img class="album__image" src="/img/post-1.jpg" alt="Вот и подошла к концу выставка «Метал-Экспо 2019»"></figure><div class="album__body"><header class="album__header"><h3 class="album__title"><a class="album__link" href="/single.html" title="Вот и подошла к концу выставка «Метал-Экспо 2019»">Вот и подошла к концу выставка «Метал-Экспо 2019»</a></h3></header><div class="album__meta"><time class="album__date">01.11.2019</time></div></div></article></li><li class="albums__item col-4"><article class="album"><figure class="album__figure"><img class="album__image" src="/img/post-2.jpg" alt="История “Как мы доставляли продукцию в Европу”"></figure><div class="album__body"><header class="album__header"><h3 class="album__title"><a class="album__link" href="/single.html" title="История “Как мы доставляли продукцию в Европу”">История “Как мы доставляли продукцию в Европу”</a></h3></header><div class="album__meta"><time class="album__date">01.11.2019</time></div></div></article></li>';

const classList = {
  el: '.js-albums',
  list: '.js-albums-list',
  footer: '.js-albums-footer',
  button: '.js-albums-button'
};

export default class Albums extends Component {
  init() {
    this._loading = false;
    this.url = this.$el.data('albumsUrl');
    this.dev = this.$el.data('useMockData');

    if (!this.url && !this.dev) {
      this.$footer.remove();
    }
  }
  events() {
    this.$button.on('click', () => {
      this.loading = true;
      this.load().then(() => {
        this.loading = false;
      });
    });
  }
  load() {
    return new Promise((resolve) => {
      if (this.url) {
        $.ajax(this.url, (response) => {
          const { html = '', isLast = true } = response;

          this.$list.append(html);

          if (isLast) {
            this.$footer.remove();
          }

          resolve();
        });
      } else {
        setTimeout(() => {
          this.$list.append(mockHTML);
          this.$footer.remove();

          resolve();
        }, 2000);
      }
    });
  }
  get loading() {
    return this._loading;
  }
  set loading(state) {
    this._loading = state;
    this.$el[state ? 'addClass' : 'removeClass']('is-loading');
  }
}

Component.mount(Albums, {
  name: 'Albums',
  state: {},
  classList
});
