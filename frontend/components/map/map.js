/**
 * Map
 */

import $ from 'jquery';
import Component from '../../js/lib/component';

const classList = {
  el: '.js-map',
  container: '.js-map-container'
};

$.getScript('//api-maps.yandex.ru/2.1/?lang=ru_RU', () => {
  ymaps.ready(() => {
    Component.mount(Map, {
      name: 'Map',
      classList,
      state: {
        init: false
      }
    });
  });
});

export default class Map extends Component {
  init() {
    this.param = {};
    this.collections = [];

    this.setParams();
    this.initMap();
  }
  setParams(data) {
    const param = data || this.$el.data('options');

    this.markers = param.markers || [];
    this.param.center = param.center || [54.765961, 32.033613];
    this.param.zoom = param.zoom || 15;
  }
  initMap() {
    this.$container.html('');

    this.map = new ymaps.Map(
      this.$container.get(0),
      {
        center: this.param.center,
        zoom: this.param.zoom,
        controls: []
      },
      {
        searchControlProvider: false
      }
    );

    const zoomControl = new ymaps.control.ZoomControl({
      options: {
        size: 'small',
        position: {
          left: 'auto',
          right: 20,
          top: 'auto',
          bottom: 40
        }
      }
    });

    this.map.controls.add(zoomControl);
    this.map.behaviors.disable('scrollZoom');
    this.generateMarkers();
    this.state.init = true;
  }
  generateMarkers() {
    this.markers.forEach((marker) => {
      this.setMarker(marker.title, marker.coords, marker.image);
    });
  }
  setMarker(title, coords, icon) {
    const marker = new ymaps.Placemark(
      coords,
      {
        hintContent: 'Адрес',
        balloonContent: title
      },
      {
        iconImageHref: icon.href || '/img/marker.svg',
        iconLayout: 'default#image',
        iconImageSize: icon.size || [56, 97],
        iconImageOffset: icon.offset || [-28, -94]
      }
    );

    this.map.geoObjects.add(marker);
    this.collections.push(marker);
  }
}
