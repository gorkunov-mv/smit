/**
 * Posts
 */

import $ from 'jquery';
import Component from '../../js/lib/component';

const mockHTML =
  '<li class="posts__item col-6"><article class="post"><figure class="post__figure"><img class="post__image" src="/img/post-1.jpg" alt="Вот и подошла к концу выставка «Метал-Экспо 2019»"></figure><div class="post__body"><header class="post__header"><h3 class="post__title"><a class="post__link" href="/post.html" title="Вот и подошла к концу выставка «Метал-Экспо 2019»">Вот и подошла к концу выставка «Метал-Экспо 2019»</a></h3></header><div class="post__meta"><time class="post__date">01.11.2019</time></div></div></article></li><li class="posts__item col-6"><article class="post"><figure class="post__figure"><img class="post__image" src="/img/post-2.jpg" alt="История “Как мы доставляли продукцию в Европу”"></figure><div class="post__body"><header class="post__header"><h3 class="post__title"><a class="post__link" href="/post.html" title="История “Как мы доставляли продукцию в Европу”">История “Как мы доставляли продукцию в Европу”</a></h3></header><div class="post__meta"><time class="post__date">01.11.2019</time></div></div></article></li>';

const classList = {
  el: '.js-posts',
  list: '.js-posts-list',
  footer: '.js-posts-footer',
  button: '.js-posts-button'
};

export default class Posts extends Component {
  init() {
    this._loading = false;
    this.url = this.$el.data('postsUrl');
    this.dev = this.$el.data('useMockData');

    if (!this.url && !this.dev) {
      this.$footer.remove();
    }
  }
  events() {
    this.$button.on('click', () => {
      this.loading = true;
      this.load().then(() => {
        this.loading = false;
      });
    });
  }
  load() {
    return new Promise((resolve) => {
      if (this.url) {
        $.ajax(this.url, (response) => {
          const { html = '', isLast = true } = response;

          this.$list.append(html);

          if (isLast) {
            this.$footer.remove();
          }

          resolve();
        });
      } else {
        setTimeout(() => {
          this.$list.append(mockHTML);
          this.$footer.remove();

          resolve();
        }, 2000);
      }
    });
  }
  get loading() {
    return this._loading;
  }
  set loading(state) {
    this._loading = state;
    this.$el[state ? 'addClass' : 'removeClass']('is-loading');
  }
}

Component.mount(Posts, {
  name: 'Posts',
  state: {},
  classList
});
