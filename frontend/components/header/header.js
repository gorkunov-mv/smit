import Component from "../../js/lib/component";

const classList = {
  el: ".js-header",
};

class Header extends Component {
  init() {
    this.set({
      height: {
        default: this.$el.outerHeight(),
        fixed: this.$el.outerHeight(),
      },
    });
  }
  events() {
    this.$window.on("scroll", () => this.handler());
    this.$document.ready(() => this.handler());
  }
  handler() {
    const offset = window.pageYOffset;
    const state = this.state.fixed;
    const bound = this.state.height.default - this.state.height.fixed;

    if (offset > bound && !state) {
      this.fix();
    }

    if (offset <= bound && state) {
      this.unfix();
    }
  }
  fix() {
    this.$el.addClass("is-fixed");
    this.set({ fixed: true });
  }
  unfix() {
    this.$el.removeAttr("style").removeClass("is-fixed");
    this.set({ fixed: false });
  }
  get isFixed() {
    return this.state.fixed;
  }
}

export default Component.mount(Header, {
  name: "Header",
  classList,
  singleton: true,
  state: {
    fixed: false,
  },
});
