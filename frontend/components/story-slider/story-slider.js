/**
 * story-slider
 */

import Component from "../../js/lib/component";
import Swiper from "swiper";

const classList = {
  el: ".js-story-slider",
  container: ".js-story-slider-container",
  list: ".js-story-slider-list",
  item: ".js-story-slider-item",
  prev: ".js-story-slider-prev",
  next: ".js-story-slider-next",
  counter: ".js-story-slider-counter",
};

export default class StorySlider extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: "auto",
        spaceBetween: 50,
        loop: true,
        centeredSlides: true,
        speed: 400,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        navigation: {
          prevEl: this.$prev.get(0),
          nextEl: this.$next.get(0),
        },
        on: {
          init: () => {
            this.$el.addClass("is-inited");
          },
        },
      },
      this.$el.data("options")
    );

    this.swiper = new Swiper(this.$container.get(0), this.options);
  }
  events() {
    this.swiper.on("slideChange", () => {
      this.updateCounter(this.swiper.realIndex + 1, this.$item.length);
    });
  }
  updateCounter(current, slidesAmount) {
    this.$counter.text(`${current} / ${slidesAmount}`);
  }
}

Component.mount(StorySlider, {
  name: "StorySlider",
  state: {},
  classList,
});
