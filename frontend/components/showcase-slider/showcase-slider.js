/**
 * showcase-slider
 */

import Component from "../../js/lib/component";
import Swiper from "swiper";

const classList = {
  el: ".js-showcase-slider",
  container: ".js-showcase-slider-container",
  list: ".js-showcase-slider-list",
  item: ".js-showcase-slider-item",
  title: ".js-showcase-slider-title",
  text: ".js-showcase-slider-text",
  prev: ".js-showcase-slider-prev",
  next: ".js-showcase-slider-next",
  counter: ".js-showcase-slider-counter",
};

export default class ShowcaseSlider extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        speed: 400,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        navigation: {
          prevEl: this.$prev.get(0),
          nextEl: this.$next.get(0),
        },
        on: {
          init: () => {
            setTimeout(() => {
              this.changeHandler().then(() => {
                this.$el.addClass("is-inited");
              });
            });
          },
        },
      },
      this.$el.data("options")
    );

    this.swiper = new Swiper(this.$container.get(0), this.options);
  }
  events() {
    this.swiper.on("slideChange", () => {
      this.changeHandler();
    });
  }
  changeHandler() {
    const currentIndex = this.swiper.realIndex;
    const $current = $(this.$item[currentIndex]);
    const { title = "", text = "" } = $current.data();

    this.updateCounter(currentIndex + 1, this.$item.length);

    return this.updateContent({ title, text });
  }
  updateCounter(current, slidesAmount) {
    this.$counter.text(`${current} / ${slidesAmount}`);
  }
  updateContent({ title, text }) {
    return Promise.all([
      this.updateText(this.$title, title),
      this.updateText(this.$text, text),
    ]);
  }
  updateText($target, text) {
    return new Promise((resolve) => {
      $target.fadeOut(() => {
        $target.text(text);

        if (text) {
          $target.fadeIn(() => resolve());
        } else {
          resolve();
        }
      });
    });
  }
}

Component.mount(ShowcaseSlider, {
  name: "ShowcaseSlider",
  state: {},
  classList,
});
