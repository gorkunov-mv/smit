/**
 * product-slider
 */

import Component from "../../js/lib/component";
import Swiper from "swiper";

const classList = {
  el: ".js-product-slider",
  container: ".js-product-slider-container",
  list: ".js-product-slider-list",
  item: ".js-product-slider-item",
  prev: ".js-product-slider-prev",
  next: ".js-product-slider-next",
  counter: ".js-product-slider-counter",
};

export default class ProductSlider extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: 1,
        loop: false,
        speed: 400,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        fadeEffect: {
          crossFade: true,
        },
        effect: "fade",
        navigation: {
          prevEl: this.$prev.get(0),
          nextEl: this.$next.get(0),
        },
        on: {
          init: () => {
            this.$el.addClass("is-inited");
          },
        },
      },
      this.$el.data("options")
    );

    this.swiper = new Swiper(this.$container.get(0), this.options);
  }
  events() {
    this.swiper.on("slideChange", () => {
      const index = this.swiper.activeIndex;
      const slidesAmount = this.swiper.slides.length;

      this.updateCounter(index + 1, slidesAmount);
    });
  }
  updateCounter(current, slidesAmount) {
    this.$counter.text(`${current} / ${slidesAmount}`);
  }
}

Component.mount(ProductSlider, {
  name: "ProductSlider",
  state: {},
  classList,
});
