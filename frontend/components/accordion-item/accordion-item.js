/**
 * accordion-item
 */

import Component from "../../js/lib/component";

const classList = {
  el: ".js-accordion-item",
  header: ".js-accordion-item-header",
  body: ".js-accordion-item-body"
};

export default class AccordionItem extends Component {
  init() {
    if (this.$el.is(".is-open")) {
      this.$body.show();
    }
  }
  events() {
    this.$header.on({
      click: () => this.toggle(),
      keypress: () => this.toggle()
    });
  }
  toggle() {
    this.$body.slideToggle();
    this.$el.toggleClass("is-open");
  }
}

Component.mount(AccordionItem, {
  name: "AccordionItem",
  classList,
  state: {}
});
