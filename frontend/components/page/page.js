import Component from '../../js/lib/component';

const keys = {
  ESC: 27
};

const subscribers = {
  onEscPress: [],
  onClickPastTarget: []
};

const breakpoint = {
  lg: 1599,
  xl: 1439,
  l: 1179,
  md: 959,
  m: 767,
  s: 559,
  xs: 320
};

const focusableList = 'a, button, input, textarea, select, textarea';
const $focusableList = $(focusableList);

class Page extends Component {
  init() {
    this.breakpoint = breakpoint;
  }
  events() {
    this.$window.on('keydown', (e) => {
      if (e.keyCode === keys.ESC) {
        subscribers.onEscPress.forEach((fn) => {
          fn(e);
        });
      }
    });

    this.$document.on('click', (e) => {
      const $target = $(e.target);

      subscribers.onClickPastTarget.forEach(({ selector, fn }) => {
        const isRoot = $target.is(selector);
        const isChilds = $target.closest(selector).length > 0;

        if (isRoot || isChilds) return;

        fn(e);
      });
    });
  }
  onEscPress(fn) {
    subscribers.onEscPress.push(fn);
  }
  onClickPastTarget(selector, fn) {
    subscribers.onClickPastTarget.push({ selector, fn });
  }
  bodyOverflowEnable() {
    this.$body.css('paddingRight', this.getScrollbarWidth());
    this.$body.addClass('is-overflow');
  }
  bodyOverflowDisable() {
    this.$body.removeAttr('style');
    this.$body.removeClass('is-overflow');
  }
  getScrollbarWidth() {
    return window.innerWidth - this.$document.width();
  }
  setFocus($target) {
    $focusableList.attr('tabindex', -1);
    $target.find(focusableList).removeAttr('tabindex');
  }
  blurFocus() {
    $focusableList.removeAttr('tabindex');
  }
  get isMobile() {
    return this.breakpoint.s >= this.$window.outerWidth();
  }
  get isTablet() {
    return this.breakpoint.md >= this.$window.outerWidth();
  }
}

export default Component.mount(Page, {
  name: 'Page',
  classList: { el: '.js-page' },
  singleton: true,
  state: {}
});
