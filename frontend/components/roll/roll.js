/**
 * showcase
 */

import Component, { nextTick } from "../../js/lib/component";
// import Page from "../page/page";
import Swiper from "swiper";

const classList = {
  el: ".js-roll",
  body: ".js-roll-body",
  swiper: ".js-roll-swiper",
  list: ".js-roll-list",
  item: ".js-roll-item"
  // prev: ".js-roll-prev",
  // next: ".js-roll-next"
};

export default class Roll extends Component {
  init() {
    this.options = Object.assign(
      {
        slidesPerView: 3,
        slidesPerGroup: 1,
        spaceBetween: 30,
        speed: 1000,
        wrapperClass: classList.list.substr(1),
        slideClass: classList.item.substr(1),
        // navigation: {
        //   prevEl: this.$prev.get(0),
        //   nextEl: this.$next.get(0)
        // },
        breakpoints: {},
        on: {
          init: () => {
            this.showControls();
            this.$el.addClass("is-inited");
          }
        }
      },
      this.$el.data("options")
    );

    nextTick().then(() => {
      this.swiper = new Swiper(this.$swiper.get(0), this.options);
    });
  }
  showControls() {
    if (this.$item.length > this.options.slidesPerView) {
      this.$prev.addClass("is-visible");
      this.$next.addClass("is-visible");
    }
  }
}

Component.mount(Roll, {
  name: "Roll",
  state: {},
  classList
});
