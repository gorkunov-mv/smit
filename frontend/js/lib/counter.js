/**
 * counter
 */

import Component from './component';
import numberFormat from './numberFormat';
import anime from 'animejs';
import $ from 'jquery';
import 'intersection-observer';

class Counter extends Component {
  init() {
    this.observer = new IntersectionObserver(
      (entries) => {
        entries.forEach(({ isIntersecting, target }) => {
          if (isIntersecting) {
            this.animate(target);
          }
        });
      },
      {
        root: null,
        rootMargin: '15%',
        threshold: 0.5
      }
    );

    this.$target.each((i, target) => {
      this.observer.observe(target);
      target.innerHTML = '0';
      target.setAttribute('data-counter-ready', 'true');
    });
  }

  format(number, separate = false) {
    return numberFormat(number, 0, ',', separate ? ' ' : '');
  }
  animate(target) {
    const state = {
      count: 0
    };

    const count = parseInt(target.dataset.counter);
    const duration = parseInt(target.dataset.duration) || 1000;
    const step = parseInt(target.dataset.counterStep || 0);
    const easing = target.dataset.counterEasing || 'easeInOutQuad';

    anime({
      targets: state,
      count,
      duration,
      easing,
      update: () => {
        const value = Math.trunc(state.count);
        const currentValue = parseInt(target.dataset.counterValue) || 0;

        if (value - currentValue > step) {
          target.innerHTML = this.format(value, target.dataset.counterSeparate);
          target.setAttribute('data-counter-value', value);
        }
      },
      complete: () => {
        const value = Math.trunc(state.count);
        target.innerHTML = this.format(value);
        target.setAttribute('data-counter-value', value);
      }
    });
  }
}

export default Component.mount(Counter, {
  name: 'Counter',
  singleton: true,
  classList: {
    el: '.js-page',
    target: '[data-counter]'
  }
});
