/**
 * @FocusController
 */

import $ from "jquery";

const FocusController = {
  $document: $(document),
  isClicked: false,
  init() {
    this.$document.on("mousedown", e => {
      this.isClicked = true;

      this.$document.one("mouseup", e => {
        this.isClicked = false;
      });
    });

    this.$document.on("click", e => {
      $(e.target).removeClass("is-focused");
    });

    this.$document.on("focusin", e => {
      if (!this.isClicked) {
        $(e.target).addClass("is-focused");
      }
    });

    this.$document.on("focusout", e => {
      $(e.target).removeClass("is-focused");
    });
  }
};

FocusController.init();
