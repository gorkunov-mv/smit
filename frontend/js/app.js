import "@babel/polyfill";
import $ from "jquery";

import "./lib/svg";
import "./lib/objectFit";
import "./lib/hello";
import "./lib/focus";
import "./lib/scroll";
import "./lib/counter";

import "../components/slider/slider";
import "../components/catalog-slider/catalog-slider";
import "../components/map/map";
import "../components/posts/posts";
import "../components/albums/albums";
import "../components/gallery/gallery";
import "../components/story-slider/story-slider";
import "../components/gallery-slider/gallery-slider";
import "../components/showcase-slider/showcase-slider";
import "../components/reviews-slider/reviews-slider";
import "../components/product-slider/product-slider";
import "../components/header/header";
import "../components/modal/modal";

/* Add jQuery to global scope */
window.$ = $;
window.jQuery = jQuery;
